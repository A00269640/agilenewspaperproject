import junit.framework.TestCase;

public class InvoiceTest extends TestCase {
	
	//Test #: 1
	//Test Objective: To create a Invoice Account
	//Inputs: phoneNo = "0874567658", lName = "Clooney", invoice_date = "01/01/2021"
	//Expected Output: Invoice Object created with id = 0, phoneNo = "0874567658", lName = "Clooney", invoice_date = "01/01/2021"
	
	public void testInvoice001() {
		

		
		
		try {

			Invoice InvoiceObj = new Invoice("0874567658", "Clooney", "01/01/2021");
			

			assertEquals(0, InvoiceObj.getId());
			assertEquals("0874567658", InvoiceObj.getPhoneNo());
			assertEquals("Clooney", InvoiceObj.getLName());
			assertEquals("01/01/2021", InvoiceObj.getInvoice_date());
			
		}
		catch (InvoiceExceptionHandler e) {
			fail("Exception not expected");
		}
		
	}
	
	//Test #: 2
	//Test Objective: To catch an invalid Invoice phoneNo
	//Inputs: phoneNo = "087"
	//Expected Output: Exception Message: "phoneNo does not meet minimum length requirements"

	public void testValidatephoneNo001() {
			
		try {
				
			//Call method under test
			Invoice.validatephoneNo("087");
			fail("Exception expected");
			fail("Exception expected");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("phoneNo does not meet minimum length requirements", e.getMessage());
		}
	}
	//Test #: 3
	//Test Objective: To catch an invalid Invoice phoneNo
	//Inputs: phoneNo = "088784174352"
	//Expected Output: Exception Message: "phoneNo exceeds maximum length requirements"

	public void testValidatephoneNo002() {

		try {

			//Call method under test
			Invoice.validatephoneNo("088784174352");
			fail("Exception expected");
			fail("Exception expected");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("phoneNo exceeds maximum length requirements", e.getMessage());
		}
	}

	//Test #: 4
	//Test Objective: To catch an invalid Invoice phoneNo
	//Inputs: phoneNo = ""
	//Expected Output: Exception Message: "phoneNo NOT specified"

	public void testValidatephoneNo003() {

		try {

			//Call method under test
			Invoice.validatephoneNo("");
			fail("Exception expected");
			fail("Exception expected");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("phoneNo NOT specified", e.getMessage());
		}
	}

	//Test #: 5
	//Test Objective: To test a correct validate
	//Inputs: phoneNo = "0874567658" lName = "Clooney"
	//Expected Output: Tests Succeed

	public void testValidatelName001() {

		try {

			//Call method under test
			Invoice.validatephoneNo("0874567658");
			Invoice.validatelName("Clooney");
			Invoice.validateinvoice_date("01/01/2020");
		}
		catch (InvoiceExceptionHandler e) {
			fail("Exception expected");
		}
	}

	//Test #: 6
	//Test Objective: To catch an invalid Invoice lName
	//Inputs: phoneNo = "0874567658" lName = "Clo"
	//Expected Output: Exception Message: "Invoice lName does not meet minimum length requirements"

	public void testValidatelName002() {

		try {

			//Call method under test
			Invoice.validatephoneNo("0874567658");
			Invoice.validatelName("Cl");
			fail("Exception expected");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("Invoice lName does not meet minimum length requirements", e.getMessage());
		}
	}

	//Test #: 7
	//Test Objective: To catch an invalid Invoice lName
	//Inputs: phoneNo = "0874567658" lName = "Clooneyyyyyyyyyyyyyyyyyyyyyyyyyy"
	//Expected Output: Exception Message: "Invoice lName exceeds maximum length requirements"

	public void testValidatelName003() {

		try {

			//Call method under test
			Invoice.validatephoneNo("0874567658");
			Invoice.validatelName("Clooneyyyyyyyyyyyyyyyyyyyyyyyyyy");
			fail("Exception expected");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("Invoice lName exceeds maximum length requirements", e.getMessage());
		}
	}

	//Test #: 7
	//Test Objective: To catch an invalid Invoice lName
	//Inputs: phoneNo = "0874567658" lName = ""
	//Expected Output: Exception Message: "Invoice lName NOT specified"

	public void testValidatelName004() {

		try {

			//Call method under test
			Invoice.validatephoneNo("0874567658");
			Invoice.validatelName("");
			fail("Exception expected");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("Invoice lName NOT specified", e.getMessage());
		}
	}

	//Test #: 7
	//Test Objective: To catch an invalid Invoice date
	//Inputs: phoneNo = "0874567658" lName = "Clooney" invoice_date = "01/01"
	//Expected Output: Exception Message: "Invoice date does not meet minimum length requirements"

	public void testValidateinvoice_date001() {

		try {

			//Call method under test
			Invoice.validatephoneNo("0874567658");
			Invoice.validatelName("Clooney");
			Invoice.validateinvoice_date("01/01");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("Invoice date does not meet minimum length requirements", e.getMessage());
		}
	}

	//Test #: 7
	//Test Objective: To catch an invalid Invoice lName
	//Inputs: phoneNo = "0874567658" lName = "Clooney" invoice_date = "01/01/2021"
	//Expected Output: Exception Message: "Invoice lName NOT specified"

	public void testValidateinvoice_date002() {

		try {

			//Call method under test
			Invoice.validatephoneNo("0874567658");
			Invoice.validatelName("Clooney");
			Invoice.validateinvoice_date("01/01/202100000000000");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("Invoice date exceeds maximum length requirements", e.getMessage());
		}
	}

	//Test #: 7
	//Test Objective: To catch an invalid Invoice lName
	//Inputs: phoneNo = "0874567658" lName = "Clooney" invoice_date = "01/01/2021"
	//Expected Output: Exception Message: "Invoice lName NOT specified"

	public void testValidateinvoice_date003() {

		try {

			//Call method under test
			Invoice.validatephoneNo("0874567658");
			Invoice.validatelName("Clooney");
			Invoice.validateinvoice_date("");
		}
		catch (InvoiceExceptionHandler e) {
			assertEquals("Invoice date NOT specified", e.getMessage());
		}
	}

}

		

