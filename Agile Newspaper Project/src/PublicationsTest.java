import junit.framework.TestCase;

public class PublicationsTest extends TestCase {

    //Test #: 1
    //Test Objective: To create a Publication Account
    //Inputs: pubTitle = "The Irish Times", pubPrice = "2.30", pubStock = "500"
    //Expected Output: Publications Object created with id = 0, pubTitle = "The Irish Times", pubPrice  = "2.30", pubStock = "500"

    public void testPublications001() {

        //Create the Publications Object

        try {

            //Call method under test
            Publications pubObj = new Publications("The Irish Times", 2.30, 500);

            // Use getters to check for object creation
            assertEquals(0, pubObj.getId());
            assertEquals("The Irish Times", pubObj.getTitle());
            assertEquals(2.30, pubObj.getPrice());
            assertEquals(500, pubObj.getStock());
        }
        catch (PublicationsExceptionHandler e) {
            fail("Exception not expected");
        }

    }


    //Test #: 2
    //Test Objective: To catch an invalid publication title(Does not meet minimum length)
    //Inputs: pubTitle = "A"
    //Expected Output: Exception Message: "Publication Title does not meet minimum length requirements"

    public void testValidateTitle001() {

        try {

            //Call method under test
            Publications.validateTitle("A");
            fail("Exception expected");
        }
        catch (PublicationsExceptionHandler e) {
            assertEquals("Publication Title does not meet minimum length requirements", e.getMessage());
        }
    }

    //Test #: 3
    //Test Objective: To catch an invalid publication title(Exceeds maximum length)
    //Inputs: pubTitle = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    //Expected Output: Exception Message: "Publication Title exceeds maximum length requirements"

    public void testValidateTitle002() {

        try {

            //Call method under test
            Publications.validateTitle("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            fail("Exception expected");
        }
        catch (PublicationsExceptionHandler e) {
            assertEquals("Publication Title exceeds maximum length requirements", e.getMessage());
        }
    }

    //Test #: 4
    //Test Objective: To use a valid publication title
    //Inputs: pubTitle = "The Irish Times"
    //Expected Output: "The Irish Times"

    public void testValidateTitle003() {

        try {

            //Call method under test
            Publications.validateTitle("The Irish Times");
        }
        catch (PublicationsExceptionHandler e) {
            fail("Exception not expected");
        }
    }

    //Test #: 5
    //Test Objective: To catch an invalid publication price
    //Inputs: pubPrice = 0.00
    //Expected Output: Exception Message: "Publication Price must be more than 0"

    public void testValidatePrice001() {

        try {

            //Call method under test
            Publications.validatePrice(0.00);
            fail("Exception expected");
        }
        catch (PublicationsExceptionHandler e) {
            assertEquals("Publication Price must be more than 0", e.getMessage());
        }
    }

    //Test #: 6
    //Test Objective: To use a valid publication price
    //Inputs: pubPrice = 2.30
    //Expected Output: 2.30

    public void testValidatePrice002() {

        try {

            //Call method under test
            Publications.validatePrice(2.30);
        }
        catch (PublicationsExceptionHandler e) {
            fail("Exception not expected");
        }
    }

    //Test #: 7
    //Test Objective: To catch an invalid publication stock
    //Inputs: pubStock = -1
    //Expected Output: Exception Message: "Publication stock can not be less than 0"

    public void testValidateStock001() {

        try {

            //Call method under test
            Publications.validateStock(-1);
            fail("Exception expected");
        }
        catch (PublicationsExceptionHandler e) {
            assertEquals("Publication stock can not be less than 0", e.getMessage());
        }
    }

    //Test #: 8
    //Test Objective: To use a valid publication stock
    //Inputs: pubStock = 500
    //Expected Output: 500

    public void testValidateStock002() {

        try {

            //Call method under test
            Publications.validateStock(500);
        }
        catch (PublicationsExceptionHandler e) {
            fail("Exception not expected");
        }
    }

}
