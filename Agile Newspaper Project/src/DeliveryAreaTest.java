import junit.framework.TestCase;

public class DeliveryAreaTest extends TestCase {
	
	//Test #: 1
	//Test Objective: To create a Delivery Area Account
	//Inputs: description = "houseAddress", houseAddress = "Athlone",
	//Expected Output: Delivery Area Object created with id = 0, "Yellow", houseAddress = "Athlone"
	
	public void testDeliveryArea001() {
		

		
		
		try {

			DeliveryArea deliveryAreaObj = new DeliveryArea("Yellow", "Athlone");
			

			assertEquals(0, deliveryAreaObj.getId());
			assertEquals("Yellow", deliveryAreaObj.getDescription());
			assertEquals("Athlone", deliveryAreaObj.getAddress());
			
		}
		catch (DeliveryAreaExceptionHandler e) {
			fail("Exception not expected");
		}
		
	}
	
	//Test #: 2
	//Test Objective: To catch an invalid DeliveryArea description
	//Inputs: description = "Y"
	//Expected Output: Exception Message: "Description does not meet minimum length requirements"

	public void testValidateDescription001() {
			
		try {
				
			//Call method under test
			DeliveryArea.validateDescription("Y");
			fail("Exception expected");
		}
		catch (DeliveryAreaExceptionHandler e) {
			assertEquals("Description does not meet minimum length requirements", e.getMessage());
		}
	}
	//Test #: 3
	//Test Objective: To catch an invalid DeliveryArea description
	//Inputs: description = "Yellow+(s*95)"
	//Expected Output: Exception Message: "Description exceeds maximum length requirements"

	public void testValidateDescription002() {

		try {

			//Call method under test
			DeliveryArea.validateDescription("Yellowsssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssssss");
			fail("Exception expected");
		}
		catch (DeliveryAreaExceptionHandler e) {
			assertEquals("Description exceeds maximum length requirements", e.getMessage());
		}
	}

	//Test #: 4
	//Test Objective: To catch an invalid DeliveryArea description
	//Inputs: description = ""
	//Expected Output: Exception Message: "Description NOT specified"

	public void testValidateDescription003() {

		try {

			//Call method under test
			DeliveryArea.validateDescription("");
			fail("Exception expected");
		}
		catch (DeliveryAreaExceptionHandler e) {
			assertEquals("Description NOT specified", e.getMessage());
		}
	}

	//Test #: 5
	//Test Objective: To test a correct DeliveryArea Address
	//Inputs: description = "Yellow" address = "Athlone"
	//Expected Output: No message expected

	public void testValidateAddress001() {

		try {

			//Call method under test
			DeliveryArea.validateDescription("yellow");
			DeliveryArea.validateAddress("Athlone");
		}
		catch (DeliveryAreaExceptionHandler e) {
			fail("Exception expected");
		}
	}

	//Test #: 6
	//Test Objective: To catch an invalid DeliveryArea address
	//Inputs: description = "Yellow" address = "At"
	//Expected Output: Exception Message: "House Address does not meet minimum length requirements"

	public void testValidateAddress002() {

		try {

			//Call method under test
			DeliveryArea.validateDescription("Yellow");
			DeliveryArea.validateAddress("Athlo");
		}
		catch (DeliveryAreaExceptionHandler e) {
			assertEquals("House Address does not meet minimum length requirements", e.getMessage());
		}
	}

	//Test #: 7
	//Test Objective: To catch an invalid DeliveryArea address
	//Inputs: description = "Yellow" address = "Athlone"
	//Expected Output: Exception Message: "House Address exceeds maximum length requirements"

	public void testValidateAddress003() {

		try {

			//Call method under test
			DeliveryArea.validateDescription("Yellow");
			DeliveryArea.validateAddress("Athlonessssssssssssssssssssssssssssssssssssssssssssssssssssss");
		}
		catch (DeliveryAreaExceptionHandler e) {
			assertEquals("House Address exceeds maximum length requirements", e.getMessage());
		}
	}

	//Test #: 7
	//Test Objective: To catch an invalid DeliveryArea address
	//Inputs: description = "Yellow" address = "Athlone"
	//Expected Output: Exception Message: "House Address exceeds maximum length requirements"

	public void testValidateAddress004() {

		try {

			//Call method under test
			DeliveryArea.validateDescription("Yellow");
			DeliveryArea.validateAddress("");
		}
		catch (DeliveryAreaExceptionHandler e) {
			assertEquals("House Address NOT specified", e.getMessage());
		}
	}

}

		

