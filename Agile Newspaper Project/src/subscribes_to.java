public class subscribes_to 
{
    public int id;
    public int id2;
    private String del_freq;
    

    void setId(int cusId) {
        id = cusId;
    }

    void setid2(int pub_id) {
        id2 = pub_id;
    }

    void setdel_freq(String del_freq) {
        del_freq = del_freq;
    }
    


    int getId() {
        return id;
    }


    int getid2() {
        return id2;
    }

    String getdel_freq() {
        return del_freq;
    }
    

    public subscribes_to(int id, int id2, String del_freq) throws CustomerExceptionHandler  {

        id = 0;
        id2 = 0;



        try {

            validatedel_freq(del_freq);


        }
        catch (CustomerExceptionHandler e) {
            throw e;
        }

        // Set Attributes
        id = id;
        id2 = id2;
        del_freq = del_freq;

    }



    public static void validatedel_freq(String del_freq) throws CustomerExceptionHandler {

        //Agree Formating Rules on "Customer del_freq"
        //E.G. id2 String must be a minimum of 5 characters and a maximum of 60 characters

        if (del_freq.isBlank() || del_freq.isEmpty())
            throw new CustomerExceptionHandler("Customer del_freq NOT specified");
        else if (del_freq.length() < 5)
            throw new CustomerExceptionHandler("Customer del_freq does not meet minimum length requirements");
        else if (del_freq.length() > 60)
            throw new CustomerExceptionHandler("Customer del_freq does not exceeds maximum length requirements");

    }
}
