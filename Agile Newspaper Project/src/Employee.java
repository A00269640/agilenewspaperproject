public class Employee {

    private int id;
    private String name;
    private String phoneNumber;
    private double rateOfPay = 0;
    private int hrsWorked = 0;

    void setId(int empId) {
        id = empId;
    }

    void setName(String empName) { name = empName; }

    void setPhoneNumber(String empPhoneNumber) {  phoneNumber = empPhoneNumber;}

    void setRateOfPay(double empRateOfPay) { rateOfPay = empRateOfPay;}

    void setHrsWorked(int empHrsWorked) {hrsWorked = empHrsWorked;}

    int getId() {
        return id;
    }

    String getName() {
        return name;
    }

    String getPhoneNumber() {
        return phoneNumber;
    }

    double getRateOfPay(){return rateOfPay;}

    int getHrsWorked(){return hrsWorked;}

    public Employee(String empName,  int empHrsWorked, double empRateOfPay, String empPhoneNumber) throws EmployeeExceptionHandler  {

        id = 0;

        // Validate Input
        try {

            validateName(empName);
            validatePhoneNumber(empPhoneNumber);
            validateRateOfPay(empRateOfPay);
            validateHrsWorked(empHrsWorked);

        }
        catch (EmployeeExceptionHandler e) {
            throw e;
        }

        // Set Attributes
        name = empName;
        phoneNumber = empPhoneNumber;
        rateOfPay = empRateOfPay;
        hrsWorked = empHrsWorked;
    }

    public static void validateName(String empName) throws EmployeeExceptionHandler {

        //Agree Formatting Rules on "Employee Name"
        //E.G. Name String must be a minimum of 2 characters and a maximum of 50 characters

        if (empName.isBlank() || empName.isEmpty())
            throw new EmployeeExceptionHandler("Employee Name NOT specified");
        else if (empName.length() < 2)
            throw new EmployeeExceptionHandler("Employee Name does not meet minimum length requirements");
        else if (empName.length() > 50)
            throw new EmployeeExceptionHandler("Employee Name exceeds maximum length requirements");

    }

    public static void validatePhoneNumber(String empPhoneNumber) throws EmployeeExceptionHandler {

        //Agree Formatting Rules on "Employee PhoneNumber"
        //E.G. PhoneNumber String must be a minimum of 5 characters and a maximum of 15 characters

        if (empPhoneNumber.isBlank() || empPhoneNumber.isEmpty())
            throw new EmployeeExceptionHandler("Employee Phone Number NOT specified");
        else if (empPhoneNumber.length() < 5)
            throw new EmployeeExceptionHandler("Employee Phone Number does not meet minimum length requirements");
        else if (empPhoneNumber.length() > 15)
            throw new EmployeeExceptionHandler("Employee Phone Number exceeds maximum length requirements");
    }

    public static void validateRateOfPay(double empRateOfPay) throws EmployeeExceptionHandler {

        //Agree Formatting Rules on "Employee RateOfPay"
        //E.G. Rate of pay double must be a minimum of 10.20

        if (empRateOfPay < 10.20)
            throw new EmployeeExceptionHandler("Employee Rate Of Pay can not be less than 10.20");
    }

    public static void validateHrsWorked(int empHrsWorked) throws EmployeeExceptionHandler {

        //Agree Formatting Rules on "Employee HrsWorked"
        //E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters

        if (empHrsWorked < 0)
            throw new EmployeeExceptionHandler("Employee HrsWorked can not be less than 0");
    }

}
