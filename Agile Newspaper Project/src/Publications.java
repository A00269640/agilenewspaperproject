

public class Publications {
	
	private int id;
	private String title;
	private double price = 0.0;
	private int stock = 0;
	
	void setId(int pubId) {
		id = pubId;
	}
	
	void setName(String pubTitle) {
		title = pubTitle;
	}
	
	void setAddress(double pubPrice) {
		price = pubPrice;
	}
	
	void setStock(int pubStock) {
		stock = pubStock;
	}

	
	int getId() {
		return id;
	}
	
	String getTitle() {
		return title;
	}
	
	double getPrice() {
		return price;
	}
	
	int getStock()
	{
		return stock;
	}
	
	
	public Publications(String pubTitle, double pubPrice, int pubStock) throws PublicationsExceptionHandler  {
		
		id = 0;
		
		// Validate Input
		try {
			
			validateTitle(pubTitle);
			validatePrice(pubPrice);
			validateStock(pubStock);
			
		}
		catch (PublicationsExceptionHandler e) {
			throw e;
		}
		
		// Set Attributes
		title = pubTitle;
		price = pubPrice;
		stock = pubStock;
	}
	
	public static void validateTitle(String pubTitle) throws PublicationsExceptionHandler {
		
		//Agree Formatting Rules on "Publication Title"
		//E.G. Title String must be a minimum of 2 characters and a maximum of 50 characters
		
		if (pubTitle.isBlank() || pubTitle.isEmpty())
			throw new PublicationsExceptionHandler("Publication Title NOT specified");
		else if (pubTitle.length() < 2)
			throw new PublicationsExceptionHandler("Publication Title does not meet minimum length requirements");
		else if (pubTitle.length() > 50)
			throw new PublicationsExceptionHandler("Publication Title exceeds maximum length requirements");
		
	}
	
	public static void validatePrice(double pubPrice) throws PublicationsExceptionHandler {
		
		//Agree Formatting Rules on "Publication Price"
		//E.G. Price Double must be a more than 0
		
		if (pubPrice == 0.0)
			throw new PublicationsExceptionHandler("Publication Price must be more than 0");
	}
	
	public static void validateStock(int pubStock) throws PublicationsExceptionHandler {

		//Agree Formatting Rules on "Publication Stock"
		//E.G. Stock Int must be more than 0

		if (pubStock < 0)
			throw new PublicationsExceptionHandler("Publication stock can not be less than 0");
	}

}
