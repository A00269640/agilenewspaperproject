
public class MySQLAccessExceptionHandler extends Exception
{
	String message;
	
	public MySQLAccessExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}
}
