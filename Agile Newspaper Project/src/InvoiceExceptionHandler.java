public class InvoiceExceptionHandler extends Throwable {
    String message;

    public InvoiceExceptionHandler(String errMessage){
        message = errMessage;
    }

    public String getMessage() {
        return message;
    }
}
