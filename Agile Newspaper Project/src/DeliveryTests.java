import junit.framework.TestCase;

public class DeliveryTests extends TestCase
{
    //Test #: 1
    //Test Objective: To create a Delivery Account
    //Inputs: delDate = "06/03/2021", delQuan = "2", cusId = "1"
    //Expected Output: Delivery Object created with id = 0, "06/03/2021", delQuan = "2"

    public void testDelivery001 () {
        try {
            //Call method under test

            Delivery delObj = new Delivery("06/03/2021", 2, 1);


            assertEquals(0, delObj.getId());
            assertEquals("06/03/2021", delObj.getDate());
            assertEquals("2", delObj.getQuantity());
        } catch (DeliveryExceptionHandler deliveryExceptionHandler) {
            fail("Exception Not Expected");
        }
    }

        //Test #: 2
        //Test Objective: To catch an invalid Delivery Date less than 10 chars
        //Inputs: cusDate = "6/3/21"
        //Expected Output: Exception Message: "Delivery Date does not meet minimum length requirements"

        public void testValidateDate001 ()
        {

            try
            {


                Delivery.validateDate("6/3/21");
                fail("Exception expected");
            }
            catch (DeliveryExceptionHandler e)
            {
                assertEquals("Delivery Date does not meet minimum length requirements", e.getMessage());
            }
        }

        //Test #: 3
        //Test Objective: To catch an invalid Delivery Date more than 10 chars
        //Inputs: cusDate = "06 / 03 / 2021"
        //Expected Output: Exception Message: "Delivery Date does not meet maximum length requirements"

        public void testValidateDate002 ()
        {

            try {


                Delivery.validateDate("06 / 03 / 2021");
                fail("Exception expected");
            } catch (DeliveryExceptionHandler e)
            {
                assertEquals("Delivery Date does not exceeds maxim sh requirements", e.getMessage());
            }
        }
        //Test #: 4
        //Test Objective: To catch an invalid Delivery_Docket Quantity
        //Inputs:  Quantity = "0"
        //Expected Output: Exception Message: "Quantity can't be less than one"

        public void testValidateQuantity001() {

            try {

                //Call method under test
                Delivery.validateQuantity(0);
            }
            catch (DeliveryExceptionHandler e) {
                assertEquals("Quantity can't be less than one", e.getMessage());
            }
        }

        //Test #: 5
        //Test Objective: To catch an invalid Delivery_Docket Quantity
        //Inputs: Quantity = "100"
        //Expected Output: Exception Message: "Quantity can't be over 99"

        public void testValidateQuantity002() {

            try {

                //Call method under test
                Delivery.validateQuantity(100);
            }
            catch (DeliveryExceptionHandler e) {
                assertEquals("Quantity can't be over 99", e.getMessage());
            }
        }

        //Test #: 6
        //Test Objective: To Verify The Output is correct
        //Inputs: delDate = "06/03/2021"
        //Expected Output: Exception Message: "Delivery Date Meets All Requirements"

        public void testValidateDate003 ()
        {
            try
            {
                Delivery.validateDate("06/03/2021");
                fail("Exception expected");
            } catch (DeliveryExceptionHandler e)
            {
                assertEquals("Delivery Date Meets All Requirements", e.getMessage());
            }
        }

    //Test #: 5
    //Test Objective: To Verify Quantity Output Is Correct
    //Inputs: Quantity = "2"
    //Expected Output: Exception Message: "Delivery Quantity Meets All Requirements"

    public void testValidateQuantity003() {

        try {

            //Call method under test
            Delivery.validateQuantity(2);
        }
        catch (DeliveryExceptionHandler e) {
            assertEquals("Quantity Amount Meets All Requirements", e.getMessage());
        }
    }
}

