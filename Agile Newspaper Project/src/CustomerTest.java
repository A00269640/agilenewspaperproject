import junit.framework.TestCase;

public class CustomerTest extends TestCase
{
	
	//Test #: 1
	//Test Objective: To create a Customer Account
	//Inputs: cusName = "Henry Bendwick", cusAddr = "Athlone", cusPhone = "087-123123123", zone_id = 4
	//Expected Output: Customer Object created with id = 0, "Jack Daniels", cusAddr = "Athlone", cusPhone = "087-123123123", zone_id = 4
	
	public void testCustomer001() {
		

		
		
		try
		{
			
			//Call method under test

			Customer cusObj = new Customer("Henry Bendwick", "Athlone", "087-123123123");
			

			assertEquals(0, cusObj.getId());
			assertEquals("Henry Bendwick", cusObj.getName());
			assertEquals("Athlone", cusObj.getAddress());
			assertEquals("087-123123123", cusObj.getPhoneNumber());
		}
		catch (CustomerExceptionHandler e)
		{
			fail("Exception not expected");
		}
		
	}
	
	//Test #: 2
	//Test Objective: To catch an invalid customer name less than 2 chars
	//Inputs: cusName = "L"
	//Expected Output: Exception Message: "Customer Name does not meet minimum length requirements"

	public void testValidateName001() {
			
		try {
				

			Customer.validateName("L");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e) {
			assertEquals("Customer Name does not meet minimum length requirements", e.getMessage());
		}
	}

	//Test #: 3
	//Test Objective: To catch an invalid customer name more than 50 chars
	//Inputs: cusName = "ThisIsASentenceThatShouldReachMoreThanFiftyCharactersSoToProveThatTestAreaIsCorrect"
	//Expected Output: Exception Message: "Customer Name does not meet maximum length requirements"

	public void testValidateName002()
	{

		try {


			Customer.validateName("ThisIsASentenceThatShouldReachMoreThanFiftyCharactersSoToProveThatTestAreaIsCorrect");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e)
		{
			assertEquals("Customer Name does not exceeds maximum length requirements", e.getMessage());
		}
	}
	//Test #: 4
	//Test Objective: To catch an invalid customer address less than 10 chars
	//Inputs: cusName = "False"
	//Expected Output: Exception Message: "Customer Address does not meet minimum length requirements"

	public void testValidateAddress001()
	{
		try
		{
			Customer.validateAddress("False");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e)
		{
			assertEquals("Customer Address does not meet minimum length requirements", e.getMessage());
		}
	}

	//Test #: 5
	//Test Objective: To catch an invalid customer address less than 100chars
	//Inputs: cusName = "The Length of this address must be longer than 100 characters therefore I have to type like I would if I was given an essay and must fill the input with as much nonsensical Nonsense as possible in order to reach the char limit"
	//Expected Output: Exception Message: "Customer Address does not meet maximum length requirements"

	public void testValidateAddress002()
	{
		try
		{
			Customer.validateAddress("The Length of this address must be longer than 100 characters therefore I have to type like I would if I was given an essay and must fill the input with as much nonsensical Nonsense as possible in order to reach the char limit");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e)
		{
			assertEquals("Customer Address does not exceeds maximum length requirements", e.getMessage());
		}
	}

	//Test #: 6
	//Test Objective: To catch an invalid customer Phone Number less than 10 chars
	//Inputs: cusName = "083-18723"
	//Expected Output: Exception Message: "Customer Phone Number does not meet minimum length requirements"

	public void testValidatePhoneNumber001()
	{
		try
		{
			Customer.validatePhoneNumber("083-18723");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e)
		{
			assertEquals("Customer Phone Number does not meet minimum length requirements", e.getMessage());
		}
	}

	//Test #: 5
	//Test Objective: To catch an invalid customer address less than 12 chars
	//Inputs: cusName = "083-167-12456783"
	//Expected Output: Exception Message: "Customer Phone Number does not meet maximum length requirements"

	public void testValidatePhoneNumber002()
	{
		try
		{
			Customer.validatePhoneNumber("083-167-12456783");
			fail("Exception expected");
		}
		catch (CustomerExceptionHandler e)
		{
			assertEquals("Customer Phone Number does not exceeds maximum length requirements", e.getMessage());
		}
	}
}