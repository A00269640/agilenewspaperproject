
public class PublicationsExceptionHandler extends Exception {
	
	String message;
	
	public PublicationsExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}
}
