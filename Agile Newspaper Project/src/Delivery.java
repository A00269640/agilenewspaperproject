public class Delivery
{

    private int id;
    private String delivery_date;
    private int quantity;
    private int cus_id;

    void setId(int delId) { id = delId; }

    void setDate(String Date) { delivery_date = Date; }

    void setQuantity(int Quantity) { quantity = Quantity;}
    
    void setCusId(int cusId) { cus_id = cusId; }

    int getId()
    {
        return id;
    }

    String getDate()
    {
        return delivery_date;
    }

    int getQuantity()
    {
        return quantity;
    }
    
    int getCusId()
    {
    	return cus_id;
    }

    public Delivery(String delDate, int delQuan, int cusId) throws DeliveryExceptionHandler  {

        id = 0;
        cus_id = 0;


        try {

            validateDate(delDate);
            validateQuantity(delQuan);
            validateCusId(cusId);

        }
        catch (DeliveryExceptionHandler e) {
            throw e;
        }


        delivery_date = delDate;
        quantity = delQuan;
        cus_id = cusId;
    }

    public static void validateDate (String delDate) throws DeliveryExceptionHandler
    {
        //Date String must be a minimum of 10 characters and a maximum of 10 characters

        if (delDate.isBlank() || delDate.isEmpty())
            throw new DeliveryExceptionHandler("Delivery date NOT specified");
        else if (delDate.length() < 10)
            throw new DeliveryExceptionHandler("Delivery date does not meet minimum length requirements");
        else if (delDate.length() > 20)
            throw new DeliveryExceptionHandler("Delivery date exceeds maximum length requirements");



    }

    //Quantity String must be a minimum of 1 character and a maximum of 4 characters

    public static void validateQuantity(int delQuan) throws DeliveryExceptionHandler
    {

        if (delQuan < 1)
            throw new DeliveryExceptionHandler("Quantity can't be less than one");
        else if (delQuan > 99)
            throw new DeliveryExceptionHandler("Quantity can't be over 99");

    }
    
    public static void validateCusId(int cusId) throws DeliveryExceptionHandler
    {

        if (cusId < 1)
            throw new DeliveryExceptionHandler("Customer ID can't be less than one");

    }

}
