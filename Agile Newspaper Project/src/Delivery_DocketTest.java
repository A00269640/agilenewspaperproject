import junit.framework.TestCase;

public class Delivery_DocketTest extends TestCase {

    //Test #: 1
    //Test Objective: To create a Delivery_Docket Account
    //Inputs: description = "Irish Times Delivery", quantity = 10, orderAddress = "Athlone", orderNumber = 1
    //Expected Output: Delivery Docket Object created with id = 0, "Irish Times Delivery", 10, "Athlone", 1

    public void testDelivery_Docket001() {




        try {

            Delivery_Docket Delivery_DocketObj = new Delivery_Docket("Irish Times Delivery", 10, "Athlone", 1);


            assertEquals(0, Delivery_DocketObj.getId());
            assertEquals("Irish Times Delivery", Delivery_DocketObj.getDescription());
            assertEquals(10, Delivery_DocketObj.getQuantity());
            assertEquals("Athlone", Delivery_DocketObj.getAddress());
            assertEquals(1, Delivery_DocketObj.getOrder_number());

        }
        catch (Delivery_DocketExceptionHandler e) {
            fail("Exception not expected");
        }

    }

    //Test #: 2
    //Test Objective: To Validate All Working
    //Inputs: description = "Y"
    //Expected Output: Exception Message: "Description does not meet minimum length requirements"

    public void testValidateAll001() {

        try {

            //Call method under test
            Delivery_Docket.validateDescription("Irish Times Delivery");
            Delivery_Docket.validateQuantity(10);
            Delivery_Docket.validateAddress("Athlone");
            Delivery_Docket.validateOrder_Number(1);
        }
        catch (Delivery_DocketExceptionHandler e) {
            fail("Exception not expected");
        }
    }
    //Test #: 3
    //Test Objective: To catch an invalid Delivery_Docket description
    //Inputs: description = "Irish Times Deliveryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy"
    //Expected Output: Exception Message: "Description exceeds maximum length requirements"

    public void testValidateDescription001() {

        try {

            //Call method under test
            Delivery_Docket.validateDescription("Irish Times Deliveryyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyyy");

        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Description exceeds maximum length requirements", e.getMessage());
        }
    }

    //Test #: 4
    //Test Objective: To catch an invalid Delivery_Docket description
    //Inputs: description = ""
    //Expected Output: Exception Message: "Description NOT specified"

    public void testValidateDescription002() {

        try {

            //Call method under test
            Delivery_Docket.validateDescription("");

        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Description NOT specified", e.getMessage());
        }
    }

    //Test #: 5
    //Test Objective: To test a correct Delivery_Docket description
    //Inputs: description = "Ir"
    //Expected Output: Exception Message: Description does not meet minimum length requirements

    public void testValidateDescription003() {

        try {

            //Call method under test
            Delivery_Docket.validateDescription("I");

        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Description does not meet minimum length requirements", e.getMessage());
        }
    }

    //Test #: 6
    //Test Objective: To catch an invalid Delivery_Docket address
    //Inputs:  address = "At"
    //Expected Output: Exception Message: "Order Address does not meet minimum length requirements"

    public void testValidateAddress001() {

        try {

            //Call method under test
            Delivery_Docket.validateAddress("Athlo");
        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Order Address does not meet minimum length requirements", e.getMessage());
        }
    }

    //Test #: 7
    //Test Objective: To catch an invalid Delivery_Docket address
    //Inputs: address = "Athlone"
    //Expected Output: Exception Message: "Order Address exceeds maximum length requirements"

    public void testValidateAddress002() {

        try {

            //Call method under test
            Delivery_Docket.validateAddress("Athlonessssssssssssssssssssssssssssssssssssssssssssssssssssss");
        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Order Address exceeds maximum length requirements", e.getMessage());
        }
    }

    //Test #: 8
    //Test Objective: To catch an invalid Delivery_Docket address
    //Inputs: address = ""
    //Expected Output: Exception Message: "Order Address NOT specified"

    public void testValidateAddress003() {

        try {

            //Call method under test
            Delivery_Docket.validateAddress("");
        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Order Address NOT specified", e.getMessage());
        }
    }

    //Test #: 9
    //Test Objective: To catch an invalid Delivery_Docket Quantity
    //Inputs:  Quantity = "0"
    //Expected Output: Exception Message: "Quantity can't be less than one"

    public void testValidateQuantity001() {

        try {

            //Call method under test
            Delivery_Docket.validateQuantity(0);
        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Quantity can't be less than one", e.getMessage());
        }
    }

    //Test #: 10
    //Test Objective: To catch an invalid Delivery_Docket Quantity
    //Inputs: Quantity = "100"
    //Expected Output: Exception Message: "Quantity can't be over 99"

    public void testValidateQuantity002() {

        try {

            //Call method under test
            Delivery_Docket.validateQuantity(100);
        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Quantity can't be over 99", e.getMessage());
        }
    }

    //Test #: 11
    //Test Objective: To catch an invalid Delivery_Docket address
    //Inputs: Order Number = 0
    //Expected Output: Exception Message: "Order Number can't be less than one"

    public void testValidateOrder_Number001() {

        try {

            //Call method under test
            Delivery_Docket.validateOrder_Number(0);
        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Order Number can't be less than one", e.getMessage());
        }
    }

    //Test #: 12
    //Test Objective: To catch an invalid Delivery_Docket address
    //Inputs: Order Number = 201
    //Expected Output: Exception Message: "Order Number resets after 200"
    public void testValidateOrder_Number002() {

        try {

            //Call method under test
            Delivery_Docket.validateOrder_Number(201);
        }
        catch (Delivery_DocketExceptionHandler e) {
            assertEquals("Order Number resets after 200", e.getMessage());
        }
    }
}