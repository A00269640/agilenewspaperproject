class Delivery_DocketExceptionHandler extends Exception
{
    String message;

    public Delivery_DocketExceptionHandler(String errMessage){
        message = errMessage;
    }

    public String getMessage() {
        return message;
    }
}
