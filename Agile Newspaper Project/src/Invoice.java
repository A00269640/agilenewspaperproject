public class Invoice {



        private int id;
        private String lName;
        private String phoneNo;
        private String invoice_date;

        void setId(int inv_Id)
        {
            id = inv_Id;
        }

        void setphoneNo(String InvoicephoneNo)
        {
            phoneNo = InvoicephoneNo;
        }

        void setlName(String InvoicelName)
        {
            lName = InvoicelName;
        }
        void setinvoice_date(String Invoice_date)
        {
            invoice_date = Invoice_date;
        }
        int getId ()
        {
            return id;
        }

        String getPhoneNo ()
        {
            return phoneNo;
        }

        String getLName ()
        {
            return lName;
        }
        String getInvoice_date ()
        {
            return invoice_date;
        }

        public Invoice(String Invoice_date, String InvoicelName, String InvoicephoneNo) throws InvoiceExceptionHandler
        {

            id = 0;

            // Validate Input
            try
            {

            	validateinvoice_date(Invoice_date);
            	validatelName(InvoicelName);
                validatephoneNo(InvoicephoneNo);
            }
            catch (InvoiceExceptionHandler e)
            {
                throw e;
            }

            // Set Attributes
            phoneNo = InvoicephoneNo;
            lName = InvoicelName;
            invoice_date = Invoice_date;
        }

        public static void validatephoneNo (String InvoicephoneNo) throws InvoiceExceptionHandler
        {

            //Agree Formating Rules on "Invoice phoneNo"
            //E.G. Name String must be a minimum of 2 characters and a maximum of 100 characters

            if (InvoicephoneNo.isBlank() || InvoicephoneNo.isEmpty())
                throw new InvoiceExceptionHandler("phoneNo NOT specified");
            else if (InvoicephoneNo.length() < 7)
                throw new InvoiceExceptionHandler("phoneNo does not meet minimum length requirements");
            else if (InvoicephoneNo.length() > 10)
                throw new InvoiceExceptionHandler("phoneNo exceeds maximum length requirements");

        }

        public static void validatelName (String InvoicelName) throws InvoiceExceptionHandler
        {

            //Agree Formating Rules on "Invoice lName"
            //E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters

            if (InvoicelName.isBlank() || InvoicelName.isEmpty())
                throw new InvoiceExceptionHandler("Invoice lName NOT specified");
            else if (InvoicelName.length() < 3)
                throw new InvoiceExceptionHandler("Invoice lName does not meet minimum length requirements");
            else if (InvoicelName.length() > 20)
                throw new InvoiceExceptionHandler("Invoice lName exceeds maximum length requirements");


        }

    public static void validateinvoice_date (String Invoice_date) throws InvoiceExceptionHandler
    {

        //Agree Formating Rules on "Invoice lName"
        //E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters

        if (Invoice_date.isBlank() || Invoice_date.isEmpty())
            throw new InvoiceExceptionHandler("Invoice date NOT specified");
        else if (Invoice_date.length() < 10)
            throw new InvoiceExceptionHandler("Invoice date does not meet minimum length requirements");
        else if (Invoice_date.length() > 20)
            throw new InvoiceExceptionHandler("Invoice date exceeds maximum length requirements");


    }


}
