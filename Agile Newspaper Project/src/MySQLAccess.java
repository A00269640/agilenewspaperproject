import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.ResultSet;
import java.sql.SQLException;


public class    MySQLAccess {
	
	private Connection connect = null;
	private Statement statement = null;
	private PreparedStatement preparedStatement = null;
	private ResultSet resultSet = null;
	
	final private String host ="localhost:3306";
	final private String user = "root";
	final private String password = "admin";
	
	
	public MySQLAccess() throws Exception {
		
		try {
			
			//Load MySQL Driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			//Setup the connection with the DB
			connect = DriverManager.getConnection("jdbc:mysql://" + host + "/newsagent?" + "user=" + user + "&password=" + password);
		}
		catch (SQLException sqle) {
			throw sqle;
		}
		
		
	}	

	public boolean insertCustomerDetailsAccount(Customer c) {
	
		boolean insertSucessfull = true;
	
		//Add Code here to call embedded SQL to insert Customer into DB
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into customer values (default, ?, ?, ?)");
			preparedStatement.setString(1, c.getName());
			preparedStatement.setString(2, c.getPhoneNumber());
			preparedStatement.setString(3, c.getAddress());
			
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
			sqle.printStackTrace();
		}
	
		return insertSucessfull;
		
	}// end insertCustomerDetailsAccount
	
	public boolean insertUpdatedCustomerDetailsAccount(int updateCusId, String upCusName, String upCusPhoneNo, String upCusAddress) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("UPDATE customer SET name = ?, phone_no = ?, address = ? WHERE cus_id = ?");
			
			preparedStatement.setString(1, upCusName);
			preparedStatement.setString(2, upCusPhoneNo);
			preparedStatement.setString(3, upCusAddress);
			preparedStatement.setInt(4, updateCusId);
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertUpdatedCustomerDetailsAccount

	public ResultSet retrieveAllCustomerAccounts() {
		
		//Add Code here to call embedded SQL to view Customer Details
	
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from customer");
		
		}
		catch (SQLException sqle) {
			resultSet = null;
		}
		return resultSet;
	}
	
	public boolean deleteCustomerById(int CusId) {

		boolean deleteSucessfull = true;
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (CusId == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from customer");
			else
				//Delete a particular Customer
				preparedStatement = connect.prepareStatement("delete from customer where cus_id = " + CusId);
			preparedStatement.executeUpdate();
		 
		}
		catch (SQLException sqle) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}
	
	public boolean insertPublicationsAccount(Publications p) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into publication values (default, ?, ?, ?)");
			preparedStatement.setString(1, p.getTitle());
			preparedStatement.setDouble(2, p.getPrice());
			preparedStatement.setInt(3, p.getStock());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertPublicationAccount
	
	public boolean insertPublicationsUpdate(int updatePubId, String upPubTitle, Double upPubPrice, int upPubStock) {
			
			boolean insertSucessfull = true;
		
			try {
			
				//Create prepared statement to issue SQL query to the database
				preparedStatement = connect.prepareStatement("UPDATE publication SET title = ?, price = ?, stock = ? WHERE pub_id = ?");
				preparedStatement.setString(1, upPubTitle);
				preparedStatement.setDouble(2, upPubPrice);
				preparedStatement.setInt(3, upPubStock);
				preparedStatement.setInt(4, updatePubId);
				preparedStatement.executeUpdate();
			
		 
			}
			catch (SQLException sqle) {
				insertSucessfull = false;
			}
		
			return insertSucessfull;
			
		}// end insertPublicationUpdate
	
	public ResultSet retrieveAllPublicationsAccounts() {
	
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from publication");
		
		}
		catch (SQLException sqle) {
			resultSet = null;
		}
		return resultSet;
	} // end retrieveAllPublicationAccounts

	public boolean deletePublicationsById(int pubID) {

		boolean deleteSucessfull = true;

		try {
			
			//Create prepared statement to issue SQL query to the database
			if (pubID == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from publication");
			else
				//Delete a particular Customer
				preparedStatement = connect.prepareStatement("delete from publication where pub_id = " + pubID);
			preparedStatement.executeUpdate();
		 
		}
		catch (SQLException sqle) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	} //end deletePublicationById
	
	public boolean insertGeographicalLocations(GeographicalLocation g) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into GeographicalLocations values (default, ?)");
			preparedStatement.setInt(1, g.getId());

		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertGeographicalLocation
	
	public boolean insertUpdatedGeographicalLocations(GeographicalLocation g) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("UPDATE GeographicalLocations set values (default, ?)");
			preparedStatement.setInt(1, g.getId());

		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertUpdatedGeographicalLocation
	
	public ResultSet retrieveAllGeographicalLocations() {
		
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from GeographicalLocations");
		
		}
		catch (SQLException sqle) {
			resultSet = null;
		}
		return resultSet;
	} // end retrieveAllGeographicalLocations
	
	public boolean deleteGeographicalLocationsById(int zoneId) {

		boolean deleteSucessfull = true;

		try {
			
			//Create prepared statement to issue SQL query to the database
			if (zoneId == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from GeographicalLocations");
			else
				//Delete a particular Customer
				preparedStatement = connect.prepareStatement("delete from GeographicalLocations where id = " + zoneId);
			preparedStatement.executeUpdate();
		 
		}
		catch (SQLException sqle) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	} //end deleteGeographicalLocationssById
	
	public boolean insertEmployeeDetailsAccount(Employee e) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into employee values (default, ?, ?, ?, ?)");
			preparedStatement.setString(1, e.getName());
			preparedStatement.setInt(2, e.getHrsWorked());
			preparedStatement.setDouble(3, e.getRateOfPay());
			preparedStatement.setString(4, e.getPhoneNumber());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertEmployeeDetailsAccount
	
	public boolean insertUpdatedEmployeeDetailsAccount(int updateEmpId, String upEmpName, int upEmpHrsWrked, Double upEmpRateofPay, String upEmpPhoneNo) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("update employee set name = ?, hours_worked = ?, rate_of_pay = ?, phone_no = ? where emp_id = ?");
			preparedStatement.setInt(1, updateEmpId);
			preparedStatement.setString(2, upEmpName);
			preparedStatement.setInt(3, upEmpHrsWrked);
			preparedStatement.setDouble(4, upEmpRateofPay);
			preparedStatement.setString(5, upEmpPhoneNo);
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) 
		{
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertUpdatedEmployeeDetailsAccount

	public ResultSet retrieveAllEmployeeAccounts() {
	
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from employee");
		
		}
		catch (SQLException sqle) {
			resultSet = null;
		}
		return resultSet;
	}

	public boolean deleteEmployeeById(int empId) {

		boolean deleteSucessfull = true;
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (empId == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from employee");
			else
				//Delete a particular Customer
				preparedStatement = connect.prepareStatement("delete from employee where emp_id = " + empId);
			preparedStatement.executeUpdate();
		 
		}
		catch (SQLException sqle) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}

	public boolean insertInvoiceDetailsAccount(Invoice i) {
	
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into invoice values (default, ?, ?, ?)");
			preparedStatement.setString(1, i.getInvoice_date());
			preparedStatement.setString(2, i.getLName());
			preparedStatement.setString(3, i.getPhoneNo());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertInvoiceDetailsAccount
	
	public boolean insertUpdatedInvoiceDetails(int updateInvId, String upInvName, String upInvPhoneNo, String upInvDate) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("UPDATE invoice SET invoice_date = ?, last_name = ?, phone_no = ? WHERE inv_id = ?");
			
			preparedStatement.setString(1, upInvDate);
			preparedStatement.setString(2, upInvName);
			preparedStatement.setString(3, upInvPhoneNo);
			preparedStatement.setInt(4, updateInvId);
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertUpdatedInvoiceDetailsAccount
	
	public ResultSet retrieveAllInvoices() {
		
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from invoice");
		
		}
		catch (SQLException sqle) {
			resultSet = null;
		}
		return resultSet;
	}
	
	public boolean deleteInvoiceById(int inv_Id) {

		boolean deleteSucessfull = true;
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (inv_Id == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from invoice");
			else
				//Delete a particular invoice
				preparedStatement = connect.prepareStatement("delete from invoice where inv_id = " + inv_Id);
			preparedStatement.executeUpdate();
		 
		}
		catch (SQLException sqle) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}
	
	public boolean insertDeliveryDetailsAccount(Delivery del) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into delivery values (default, ?, ?, ?)");
			preparedStatement.setString(1, del.getDate());
			preparedStatement.setInt(2, del.getQuantity());
			preparedStatement.setInt(3, del.getCusId());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertDeliveryDetailsAccount
	
	public boolean insertDeliveryDetailsAccountUpdate(int upDelId, String upDate, int upOrderQuantity, int upCusId) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("UPDATE delivery SET delivery_date = ?, quantity = ?, cus_id = ? WHERE del_id = ?");
			preparedStatement.setString(1, upDate);
			preparedStatement.setInt(2, upOrderQuantity);
			preparedStatement.setInt(3, upCusId);
			preparedStatement.setInt(4, upDelId);
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertDeliveryDetailsAccountUpdate
	
	public ResultSet retrieveAllDeliverys() {
		
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from delivery");
		
		}
		catch (SQLException sqle) {
			resultSet = null;
		}
		return resultSet;
	}
	
	public boolean deleteDeliverysById(int del_Id) {

		boolean deleteSucessfull = true;
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (del_Id == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from delivery");
			else
				//Delete a particular delivery
				preparedStatement = connect.prepareStatement("delete from delivery where del_id = " + del_Id);
			preparedStatement.executeUpdate();
		 
		}
		catch (SQLException sqle) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}
	
	public boolean insertDeliveryDocketDetailsAccount(Delivery_Docket d) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("insert into delivery_docket values (default, ?, ?, ?, ?)");
			preparedStatement.setString(1, d.getDescription());
			preparedStatement.setInt(2, d.getQuantity());
			preparedStatement.setString(3, d.getAddress());
			preparedStatement.setInt(4, d.getOrder_number());
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertDeliveryDocketDetailsAccount
	
	public boolean insertDeliveryDocketDetailsAccountUpdate(int upAreaId, String upOrderDescription, int upOrderQuantity, String upOrderAddress, int upOrderNumber) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("UPDATE delivery_docket SET description = ?, quantity = ?, address = ?, order_number = ? WHERE area_id = ?");
			preparedStatement.setString(1, upOrderDescription);
			preparedStatement.setInt(2, upOrderQuantity);
			preparedStatement.setString(3, upOrderAddress);
			preparedStatement.setInt(4, upOrderNumber);
			preparedStatement.setInt(5, upAreaId);
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertDeliveryDocketDetailsAccountUpdate
	
	public ResultSet retrieveAllDeliveryDockets() {
		
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from delivery_docket");
		
		}
		catch (SQLException sqle) {
			resultSet = null;
		}
		return resultSet;
	}
	
	public boolean deleteDeliveryDocketById(int area_Id) {

		boolean deleteSucessfull = true;
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (area_Id == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from delivery_docket");
			else
				//Delete a particular delivery docket
				preparedStatement = connect.prepareStatement("delete from delivery_docket where area_id = " + area_Id);
			preparedStatement.executeUpdate();
		 
		}
		catch (SQLException sqle) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}
	

	
	public boolean insertUpdatedBillsDetailsAccount(int upBillId, String upMethodOfPayment, String upBillDate, int upCusId) {
		
		boolean insertSucessfull = true;
	
		try {
		
			//Create prepared statement to issue SQL query to the database
			preparedStatement = connect.prepareStatement("UPDATE bills SET method_of_payment = ?, bill_date = ?, cus_id = ? WHERE bill_id = ?");
			preparedStatement.setString(1, upMethodOfPayment);
			preparedStatement.setString(2, upBillDate);
			preparedStatement.setInt(3, upCusId);
			preparedStatement.setInt(4, upBillId);
			preparedStatement.executeUpdate();
		
	 
		}
		catch (SQLException sqle) {
			insertSucessfull = false;
		}
	
		return insertSucessfull;
		
	}// end insertDeliveryDetailsAccountUpdate
	
	public ResultSet retrieveAllBillsAccounts() {
		
		try {
			statement = connect.createStatement();
			resultSet = statement.executeQuery("Select * from bills");
		
		}
		catch (SQLException sqle) {
			resultSet = null;
		}
		return resultSet;
	}
	
	public boolean deleteBillsById(int bill_Id) {

		boolean deleteSucessfull = true;
		
		try {
			
			//Create prepared statement to issue SQL query to the database
			if (bill_Id == -99)
				//Delete all entries in Table
				preparedStatement = connect.prepareStatement("delete from bill");
			else
				//Delete a particular delivery
				preparedStatement = connect.prepareStatement("delete from bills where bill_Id = " + bill_Id);
			preparedStatement.executeUpdate();
		 
		}
		catch (SQLException sqle) {
			deleteSucessfull = false;
		}
		
		return deleteSucessfull;
		
	}

}// end Class

