
public class Customer {
	
	private int id;
	private String name;
	private String address;
	private String phoneNumber;
	
	void setId(int cusId) {
		id = cusId;
	}
	
	void setName(String cusName) {
		name = cusName;
	}
	
	void setAddress(String cusAddr) {
		address = cusAddr;
	}
	
	void setPhoneNumber(String cusPhone) {
		phoneNumber = cusPhone;
	}


	int getId() {
		return id;
	}

	
	String getName() {
		return name;
	}
	
	String getAddress() {
		return address;
	}
	
	String getPhoneNumber() {
		return phoneNumber;

		
	}
	
	public Customer(String cusName, String cusAddr, String cusPhone) throws CustomerExceptionHandler  {
		
		id = 0;
		

		try {
			
			validateName(cusName);
			validateAddress(cusAddr);
			validatePhoneNumber(cusPhone);
			
		}
		catch (CustomerExceptionHandler e) {
			throw e;
		}
		
		// Set Attributes
		name = cusName;
		address = cusAddr;
		phoneNumber = cusPhone;
	}
	
	public static void validateName(String cusName) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer Name"
		//E.G. Name String must be a minimum of 2 characters and a maximum of 50 characters
		
		if (cusName.isBlank() || cusName.isEmpty())
			throw new CustomerExceptionHandler("Customer Name NOT specified");
		else if (cusName.length() < 2)
			throw new CustomerExceptionHandler("Customer Name does not meet minimum length requirements");
		else if (cusName.length() > 50)
			throw new CustomerExceptionHandler("Customer Name does not exceeds maximum length requirements");
		
	}
	
	public static void validateAddress(String cusAddr) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer Address"
		//E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters
		
		if (cusAddr.isBlank() || cusAddr.isEmpty())
			throw new CustomerExceptionHandler("Customer Address NOT specified");
		else if (cusAddr.length() < 5)
			throw new CustomerExceptionHandler("Customer Address does not meet minimum length requirements");
		else if (cusAddr.length() > 60)
			throw new CustomerExceptionHandler("Customer Address does not exceeds maximum length requirements");
		
	}
	
	public static void validatePhoneNumber(String cusPhone) throws CustomerExceptionHandler {
		
		//Agree Formating Rules on "Customer PhoneNumber"
		//E.G. Name String must be a minimum of 7 characters and a maximum of 15 characters
		
		if (cusPhone.isBlank() || cusPhone.isEmpty())
			throw new CustomerExceptionHandler("Customer PhoneNumber NOT specified");
		else if (cusPhone.length() < 7)
			throw new CustomerExceptionHandler("Customer PhoneNumber does not meet minimum length requirements");
		else if (cusPhone.length() > 15)
			throw new CustomerExceptionHandler("Customer PhoneNumber does not exceeds maximum length requirements");
		
	}
	

}
