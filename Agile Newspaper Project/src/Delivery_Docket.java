public class Delivery_Docket
{
    private int id;
    private String description;
    private int quantity;
    private String address;
    private int order_number;

    void setId(int area_Id)
    {
        id = area_Id;
    }

    void setDescription(String orderDescription)
    {
        description = orderDescription;
    }

    void setQuantity(int orderQuantity)
    {
        quantity = orderQuantity;
    }

    void setAddress(String orderAddress)
    {
        address = orderAddress;
    }

    void setOrder_number(int poNumber)
    {
        order_number = poNumber;
    }
    int getId ()
    {
        return id;
    }

    String getDescription ()
    {
        return description;
    }

    int getQuantity ()
    {
        return quantity;
    }

    String getAddress ()
    {
        return address;
    }

    int getOrder_number ()
    {
        return order_number;
    }

    public Delivery_Docket(String orderDescription, int orderQuantity, String orderAddress, int poNumber) throws Delivery_DocketExceptionHandler
    {

        id = 0;

        // Validate Input
        try
        {
            validateDescription(orderDescription);
            validateQuantity(orderQuantity);
            validateAddress(orderAddress);
            validateOrder_Number(poNumber);
        }
        catch (Delivery_DocketExceptionHandler e)
        {
            throw e;
        }

        // Set Attributes
        description = orderDescription;
        quantity = orderQuantity;
        address = orderAddress;
        order_number = poNumber;
    }

    public static void validateDescription (String orderDescription) throws Delivery_DocketExceptionHandler
    {



        if (orderDescription.isBlank() || orderDescription.isEmpty())
            throw new Delivery_DocketExceptionHandler("Description NOT specified");
        else if (orderDescription.length() < 2)
            throw new Delivery_DocketExceptionHandler("Description does not meet minimum length requirements");
        else if (orderDescription.length() > 100)
            throw new Delivery_DocketExceptionHandler("Description exceeds maximum length requirements");

    }

    public static void validateQuantity (int orderQuantity) throws Delivery_DocketExceptionHandler
    {



        if (orderQuantity < 1)
            throw new Delivery_DocketExceptionHandler("Quantity can't be less than one");
        else if (orderQuantity > 99)
            throw new Delivery_DocketExceptionHandler("Quantity can't be over 99");

    }

    public static void validateAddress (String orderAddress) throws Delivery_DocketExceptionHandler
    {


        if (orderAddress.isBlank() || orderAddress.isEmpty())
            throw new Delivery_DocketExceptionHandler("Order Address NOT specified");
        if (orderAddress.length() < 5)
            throw new Delivery_DocketExceptionHandler("Order Address does not meet minimum length requirements");
        else if (orderAddress.length() > 60)
            throw new Delivery_DocketExceptionHandler("Order Address exceeds maximum length requirements");


    }

    public static void validateOrder_Number (int poNumber) throws Delivery_DocketExceptionHandler
    {



        if (poNumber < 1)
            throw new Delivery_DocketExceptionHandler("Order Number can't be less than one");
        else if (poNumber > 200)
            throw new Delivery_DocketExceptionHandler("Order Number resets after 200");

    }

}
