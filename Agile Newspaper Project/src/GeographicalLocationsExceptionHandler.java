
public class GeographicalLocationsExceptionHandler extends Exception {
	
	String message;
	
	public GeographicalLocationsExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}
}