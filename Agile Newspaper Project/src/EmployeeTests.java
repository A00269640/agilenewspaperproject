import junit.framework.TestCase;

public class EmployeeTests extends TestCase {

    //Test #: 1
    //Test Objective: To create an Employee Account
    //Inputs: empName = "John Reeves", empPhoneNumber = "086 636 6544", empRateOfPay= "10.20", empHrsWorked = "8"
    //Expected Output: Employee Object created with id = 0, empName = "John Reeves", empPhoneNumber = "086 636 6544", empRateOfPay= "10.20", empHrsWorked = "8"

    public void testEmployee001() {

        //Create the Employee Object

        try {

            //Call method under test
            Employee empObj = new Employee("John Reeves",8,10.20,"086 636 6544");

            // Use getters to check for object creation
            assertEquals(0, empObj.getId());
            assertEquals("John Reeves", empObj.getName());
            assertEquals("086 636 6544", empObj.getPhoneNumber());
            assertEquals(10.20, empObj.getRateOfPay());
            assertEquals(8, empObj.getHrsWorked());
        }
        catch (EmployeeExceptionHandler e) {
            fail("Exception not expected");
        }

    }


    //Test #: 2
    //Test Objective: To catch an invalid employee name(Does not meet minimum length)
    //Inputs: empName = "A"
    //Expected Output: Exception Message: "Employee Name does not meet minimum length requirements"

    public void testValidateName001() {

        try {

            //Call method under test
            Employee.validateName("A");
            fail("Exception expected");
        }
        catch (EmployeeExceptionHandler e) {
            assertEquals("Employee Name does not meet minimum length requirements", e.getMessage());
        }
    }

    //Test #: 3
    //Test Objective: To catch an invalid employee name(Exceeds maximum length)
    //Inputs: empName = "AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA"
    //Expected Output: Exception Message: "Employee Name exceeds maximum length requirements"

    public void testValidateName002() {

        try {

            //Call method under test
            Employee.validateName("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA");
            fail("Exception expected");
        }
        catch (EmployeeExceptionHandler e) {
            assertEquals("Employee Name exceeds maximum length requirements", e.getMessage());
        }
    }

    //Test #: 4
    //Test Objective: To use a valid employee name
    //Inputs: empName = "John Reeves"
    //Expected Output: "John Reeves"

    public void testValidateName003() {

        try {

            //Call method under test
            Employee.validateName("John Reeves");
        }
        catch (EmployeeExceptionHandler e) {
            fail("Exception not expected");
        }
    }

    //Test #: 5
    //Test Objective: To catch an invalid employee phoneNumber(Does not meet minimum length)
    //Inputs: empPhoneNo = "0852"
    //Expected Output: Exception Message: "Employee Phone Number does not meet minimum length requirements"

    public void testValidatePhoneNo001() {

        try {

            //Call method under test
            Employee.validatePhoneNumber("0852");
            fail("Exception expected");
        }
        catch (EmployeeExceptionHandler e) {
            assertEquals("Employee Phone Number does not meet minimum length requirements", e.getMessage());
        }
    }

    //Test #: 6
    //Test Objective: To catch an invalid employee phoneNumber(Exceeds maximum length)
    //Inputs: empPhoneNo = "0852383950183945"
    //Expected Output: Exception Message: "Employee Phone Number exceeds maximum length requirements"

    public void testValidatePhoneNo002() {

        try {

            //Call method under test
            Employee.validatePhoneNumber("0852383950183945");
            fail("Exception expected");
        }
        catch (EmployeeExceptionHandler e) {
            assertEquals("Employee Phone Number exceeds maximum length requirements", e.getMessage());
        }
    }

    //Test #: 7
    //Test Objective: To use a valid employee phoneNumber
    //Inputs: empPhoneNo = "085 273 9836"
    //Expected Output: "085 273 9836"

    public void testValidatePhoneNo003() {

        try {

            //Call method under test
            Employee.validatePhoneNumber("085 273 9836");
        }
        catch (EmployeeExceptionHandler e) {
            fail("Exception not expected");
        }
    }

    //Test #: 8
    //Test Objective: To catch an invalid employee rateOfPay
    //Inputs: empRateOfPay = "10.19"
    //Expected Output: Exception Message: "Employee Rate Of Pay can not be less than 10.20"

    public void testValidateRateOfPay001() {

        try {

            //Call method under test
            Employee.validateRateOfPay(10.19);
            fail("Exception expected");
        }
        catch (EmployeeExceptionHandler e) {
            assertEquals("Employee Rate Of Pay can not be less than 10.20", e.getMessage());
        }
    }

    //Test #: 9
    //Test Objective: To use a valid employee rateOfPay
    //Inputs: empRateOfPay = "10.20"
    //Expected Output: 10.20

    public void testValidateRateOfPay002() {

        try {

            //Call method under test
            Employee.validateRateOfPay(10.20);;
        }
        catch (EmployeeExceptionHandler e) {
            assertEquals("Exception not expected", e.getMessage());
        }
    }

    //Test #: 10
    //Test Objective: To catch an invalid employee HrsWorked
    //Inputs: empHrsWorked = "-1"
    //Expected Output: Exception Message: "Employee RateOfPay can not be less than 0"

    public void testValidateHrsWorked001() {

        try {

            //Call method under test
            Employee.validateHrsWorked(-1);
            fail("Exception expected");
        }
        catch (EmployeeExceptionHandler e) {
            assertEquals("Employee HrsWorked can not be less than 0", e.getMessage());
        }
    }

    //Test #: 11
    //Test Objective: To use a valid employee HrsWorked
    //Inputs: empHrsWorked = "8"
    //Expected Output: 8

    public void testValidateHrsWorked002() {

        try {

            //Call method under test
            Employee.validateHrsWorked(2);
        }
        catch (EmployeeExceptionHandler e) {
            assertEquals("Exception not expected", e.getMessage());
        }
    }

}


