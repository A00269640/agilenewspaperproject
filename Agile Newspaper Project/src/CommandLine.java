import java.sql.Connection;
import java.sql.ResultSet;
import java.util.Scanner;

public class CommandLine {

    private static void listCustomerFunctionalityAvailable() {

        //Present Customer with Functionality Options

        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Customer Account");
        System.out.println("2. View ALL Customer Records");
        System.out.println("3. Delete Customer Record by ID");
        System.out.println("4. Update Customer Record");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }

    private static void listPublicationsFunctionalityAvailable() {

        //Present Customer with Functionality Options

        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Publications Account");
        System.out.println("2. View ALL Publications Records");
        System.out.println("3. Delete Publications Record by ID");
        System.out.println("4. Update Publications Record");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }

    private static void listGeographicalLocationFunctionalityAvailable() {

        //Present Customer with Functionality Options

        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Geographical Location");
        System.out.println("2. View ALL Geographical Locations");
        System.out.println("3. Delete Geograhical Location by ID");
        System.out.println("4. Update Geographical Location Record");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }

    private static void listEmployeeFunctionalityAvailable() {

        //Present Customer with Functionality Options

        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Employee");
        System.out.println("2. View ALL Employees");
        System.out.println("3. Delete Employee by ID");
        System.out.println("4. Update Employee Record");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }

    private static void listInvoiceFunctionalityAvailable() {

        //Present Customer with Functionality Options

        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Invoice");
        System.out.println("2. View ALL Invoices");
        System.out.println("3. Delete Invoice by ID");
        System.out.println("4. Update Invoice Record");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }
    
    private static void listDeliveryDocketFunctionalityAvailable() {

        //Present Customer with Functionality Options

        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Delivery Docket");
        System.out.println("2. View ALL Delivery Dockets");
        System.out.println("3. Delete Delivery Docket by ID");
        System.out.println("4. Update Delivery Docket");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }
    
    private static void listDeliveryFunctionalityAvailable() {

        //Present Customer with Functionality Options

        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Delivery");
        System.out.println("2. View ALL Delivery");
        System.out.println("3. Delete Delivery by ID");
        System.out.println("4. Update Delivery");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }
    
    private static void listBillsFunctionalityAvailable() {

        //Present Customer with Functionality Options

        System.out.println(" ");
        System.out.println("=============================================");
        System.out.println("Please choose ONE of the following options:");
        System.out.println("1. Create Bill");
        System.out.println("2. View ALL Bills");
        System.out.println("3. Delete Bill by ID");
        System.out.println("4. Update Bill");
        System.out.println("99. Close the NewsAgent Application");
        System.out.println("=============================================");
        System.out.println(" ");

    }
    
    


    private static boolean printCustomerTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Customer Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("cus_id");
            String name = rs.getString("name");
            String addr = rs.getString("address");
            String phone = rs.getString("phone_no");
            System.out.printf("%30s", id);
            System.out.printf("%30s", name);
            System.out.printf("%30s", phone);
            System.out.printf("%30s", addr);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }

    private static boolean printPublicationsTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Publications Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("pub_id");
            String title = rs.getString("title");
            Double price = rs.getDouble("price");
            int stock = rs.getInt("stock");
            System.out.printf("%30s", id);
            System.out.printf("%30s", title);
            System.out.printf("%30s", price);
            System.out.printf("%30s", stock);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }

    private static boolean printGeographicalLocationsTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Publications Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("id");
            int zoneId = rs.getInt("zoneId");
            System.out.printf("%30s", id);
            System.out.printf("%30s", zoneId);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }

    private static boolean printEmployeeTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Employee Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("emp_id");
            String name = rs.getString("name");
            String phoneNumber = rs.getString("phone_no");
            double rateOfPay = rs.getDouble("rate_of_pay");
            int hrsWorked = rs.getInt("hours_worked");
            System.out.printf("%30s", id);
            System.out.printf("%30s", name);
            System.out.printf("%30s", hrsWorked);
            System.out.printf("%30s", rateOfPay);
            System.out.printf("%30s", phoneNumber);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }

    private static boolean printDeliveryDocketTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Invoice Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("area_id");
            String description = rs.getString("description");
            int quantity = rs.getInt("quantity");
            String address = rs.getString("address");
            int order_number = rs.getInt("order_number");
            System.out.printf("%30s", id);
            System.out.printf("%30s", description);
            System.out.printf("%30s", quantity);
            System.out.printf("%30s", address);
            System.out.printf("%30s", order_number);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }
    
    private static boolean printDeliveryTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Delivery Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("del_id");
            String date = rs.getString("delivery_date");
            int quantity = rs.getInt("quantity");
            int cusId = rs.getInt("cus_id");
            System.out.printf("%30s", id);
            System.out.printf("%30s", date);
            System.out.printf("%30s", quantity);
            System.out.printf("%30s", cusId);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }
    
    private static boolean printInvoiceTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Invoice Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("inv_id");
            String lName = rs.getString("last_name");
            String phoneNo = rs.getString("phone_no");
            String invoice_date = rs.getString("invoice_date");
            System.out.printf("%30s", id);
            System.out.printf("%30s", invoice_date);
            System.out.printf("%30s", lName);
            System.out.printf("%30s", phoneNo);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }
    
    /*private static boolean printBillsTable(ResultSet rs) throws Exception {

        //Print The Contents of the Full Bills Table

        System.out.println("------------------------------------------------------------------------------------------------------------------------------------");
        System.out.println("Table: " + rs.getMetaData().getTableName(1));
        for (int i = 1; i <= rs.getMetaData().getColumnCount(); i++) {
            System.out.printf("%30s",rs.getMetaData().getColumnName(i));
        }
        System.out.println();
        while (rs.next()) {
            int id = rs.getInt("bill_id");
            String methodOfPayment = rs.getString("method_of_payment");
            String date = rs.getString("bill_date");
            int cusId = rs.getInt("cus_id");
            System.out.printf("%30s", id);
            System.out.printf("%30s", methodOfPayment);
            System.out.printf("%30s", date);
            System.out.printf("%30s", cusId);
            System.out.println();
        }// end while
        System.out.println("--------------------------------------------------------------------------------------------------------------------------------------");

        return true;

    }*/
    
    

    public static void main(String[] args) throws InvoiceExceptionHandler {

        //Create the Database Object

        try {

            MySQLAccess dao = new MySQLAccess();

            // Configure System for Running
            Scanner keyboard = new Scanner(System.in);
            String functionNumber = "-99";
            String selection = "";
            boolean keepAppOpen = true;

            while (keepAppOpen == true) {

                //Present list of functionality and get selection
                System.out.println("Please Select An Option: ");
                System.out.println("Press 1 For Customer Functionality: ");
                System.out.println(" ");
                System.out.println("Press 2 For Publications Functionality: ");
                System.out.println(" ");
                System.out.println("Press 3 For Employee Functionality: ");
                System.out.println(" ");
                System.out.println("Press 4 For Invoice Functionality: ");
                System.out.println(" ");
                System.out.println("Press 5 For Delivery Docket Functionality: ");
                System.out.println(" ");
                System.out.println("Press 6 For Delivery Functionality: ");
                /*System.out.println(" ");
                System.out.println("Press 7 For Bills Functionality: ");*/

                selection = keyboard.nextLine();

                if(selection.equals("1"))
                {
                    listCustomerFunctionalityAvailable();
                    functionNumber = keyboard.nextLine();

                    switch (functionNumber) {

                        case "1":
                            //Get Customer Details from the User
                            System.out.println("Enter Customer Name: \n");
                            String custName = keyboard.nextLine();
                            System.out.println("Enter Customer Address: \n");
                            String custAddr = keyboard.nextLine();
                            System.out.println("Enter Customer PhoneNumber: \n");
                            String custphoneNumber = keyboard.nextLine();

                            Customer custObj = new Customer(custName,custAddr,custphoneNumber);

                            //Insert Customer Details into the database
                            boolean insertResult = dao.insertCustomerDetailsAccount(custObj);
                            if (insertResult == true)
                                System.out.println("Customer Details Saved");
                            else
                                System.out.println("ERROR: Customer Details NOT Saved");
                            break;

                        case "2":
                            //Retrieve ALL Customer Records
                            ResultSet rSet = dao.retrieveAllCustomerAccounts();
                            if (rSet == null) {
                                System.out.println("No Records Found");
                                break;
                            }
                            else {
                                boolean tablePrinted = printCustomerTable(rSet);
                                if (tablePrinted == true)
                                    rSet.close();
                            }
                            break;

                        case "3":
                            //Delete Customer Record by ID
                            System.out.println("Enter Customer Id to be deleted or -99 to Clear all Rows");
                            String deleteCusId = keyboard.next();
                            boolean deleteResult = dao.deleteCustomerById(Integer.parseInt(deleteCusId));
                            if ((deleteResult == true) && (deleteCusId.equals("-99")))
                                System.out.println("Customer Table Emptied");
                            else if (deleteResult == true)
                                System.out.println("Customer Deleted");
                            else
                                System.out.println("ERROR: Customer Details NOT Deleted or Do Not Exist");
                            break;

                        case "4":
                            //Get Customer Details from the User For Update
                        	System.out.println("Enter Customer ID To Be Updated: \n");
                        	int updateCusId = keyboard.nextInt();
                        	keyboard.nextLine();
                        	System.out.println("Enter Customer Name: \n");
                            String upCusName = keyboard.nextLine();
                            keyboard.nextLine();
                            System.out.println("Enter Customer PhoneNumber: \n");
                            String upCusPhoneNo = keyboard.next();
                            keyboard.nextLine();
                            System.out.println("Enter Customer Address: \n");
                            String upCusAddress = keyboard.nextLine();
                            
                            //Insert Customer Details into the database
                            boolean insertUpResult = dao.insertUpdatedCustomerDetailsAccount(updateCusId, upCusName, upCusPhoneNo, upCusAddress);
                            if (insertUpResult == true)
                                System.out.println("Customer Update Saved");
                            else
                                System.out.println("ERROR: Customer Update NOT Saved");
                            break;
                            
                        case "99":
                            keepAppOpen = false;
                            System.out.println("Closing the Application");
                            break;

                        default:
                            System.out.println("No Valid Function Selected");
                            break;
                    } // end switch

                }// end if

                else if(selection.equals("2"))
                {
                    listPublicationsFunctionalityAvailable();
                    functionNumber = keyboard.nextLine();

                    switch (functionNumber) {

                        case "1":
                            //Get Publication Details from the User
                            System.out.printf("Enter Publication Title: \n");
                            String pubTitle = keyboard.next();
                            System.out.printf("Enter Publication Stock: \n");
                            int pubStock = keyboard.nextInt();
                            keyboard.nextLine();
                            System.out.printf("Enter Publication Price: \n");
                            double pubPrice = keyboard.nextDouble();

                            Publications pubObj = new Publications(pubTitle,pubPrice, pubStock);

                            //Insert Publication Details into the database
                            boolean insertResult = dao.insertPublicationsAccount(pubObj);
                            if (insertResult == true)
                                System.out.println("Publications Details Saved");
                            else
                                System.out.println("ERROR: Publications Details NOT Saved");
                            break;

                        case "2":
                            //Retrieve ALL Publications Records
                            ResultSet rSet = dao.retrieveAllPublicationsAccounts();
                            if (rSet == null) {
                                System.out.println("No Records Found");
                                break;
                            }
                            else {
                                boolean tablePrinted =  printPublicationsTable(rSet);
                                if (tablePrinted == true)
                                    rSet.close();
                            }
                            break;

                        case "3":
                            //Delete Publications by ID
                            System.out.println("Enter Publication Id to be deleted or -99 to Clear all Rows");
                            String deletePubId = keyboard.next();
                            boolean deleteResult = dao.deletePublicationsById(Integer.parseInt(deletePubId));
                            if ((deleteResult == true) && (deletePubId.equals("-99")))
                                System.out.println("Publication Table Emptied");
                            else if (deleteResult == true)
                                System.out.println("Publication Deleted");
                            else
                                System.out.println("ERROR: Publication Details NOT Deleted or Do Not Exist");
                            break;

                        case "4":
                            //Get Publication Details from the User For Update
                        	System.out.println("Enter Publication Id to be Updated: \n");
                        	int updatePubId = keyboard.nextInt();
                        	keyboard.nextLine();
                            System.out.println("Enter Updated Publication Title: \n");
                            String upPubTitle = keyboard.nextLine();
                            keyboard.nextLine();
                            System.out.println("Enter Updated Publication Stock: \n");
                            int upPubStock = keyboard.nextInt();
                            System.out.println("Enter Updated Publication Price: \n");
                            double upPubPrice = keyboard.nextDouble();
                            
                            //Insert Customer Details into the database
                            boolean insertUpResult = dao.insertPublicationsUpdate(updatePubId, upPubTitle, upPubPrice, upPubStock);
                            if (insertUpResult == true)
                                System.out.println("Publicaton Update Saved");
                            else
                                System.out.println("ERROR: Publicaton Update NOT Saved");
                            break;

                        case "99":
                            keepAppOpen = false;
                            System.out.println("Closing the Application");
                            break;

                        default:
                            System.out.println("No Valid Function Selected");
                            break;
                    } // end switch

                }// end if

                
                else if(selection.equals("3"))
                {
                    listEmployeeFunctionalityAvailable();
                    functionNumber = keyboard.nextLine();

                    switch (functionNumber) {

                        case "1":
                            //Get Employee Details from the User
                            System.out.printf("Enter Employee name: \n");
                            String empName = keyboard.next();
                            System.out.printf("Enter Employee hours worked: \n");
                            int empHrsWorked = keyboard.nextInt();
                            System.out.printf("Enter Employee rate of pay: \n");
                            double empRateOfPay = keyboard.nextDouble();
                            System.out.printf("Enter Employee phone number: \n");
                            String empPhoneNumber = keyboard.next();
                            
                            

                           Employee empObj = new Employee(empName, empHrsWorked, empRateOfPay, empPhoneNumber);

                            //Insert Employee Details into the database
                            boolean insertResult = dao.insertEmployeeDetailsAccount(empObj);
                            if (insertResult == true)
                                System.out.println("Employee Details Saved");
                            else
                                System.out.println("ERROR: Employee Details NOT Saved");
                            break;

                        case "2":
                            //Retrieve ALL Employee Records
                            ResultSet rSet = dao.retrieveAllEmployeeAccounts();
                            if (rSet == null) {
                                System.out.println("No Records Found");
                                break;
                            }
                            else {
                                boolean tablePrinted =  printEmployeeTable(rSet);
                                if (tablePrinted == true)
                                    rSet.close();
                            }
                            break;

                        case "3":
                            //Delete Employee by ID
                            System.out.println("Enter Employee Id to be deleted or -99 to Clear all Rows");
                            String deleteEmpId = keyboard.nextLine();
                            boolean deleteResult = dao.deleteEmployeeById(Integer.parseInt(deleteEmpId));
                            if ((deleteResult == true) && (deleteEmpId.equals("-99")))
                                System.out.println("Employee Table Emptied");
                            else if (deleteResult == true)
                                System.out.println("Employee Deleted");
                            else
                                System.out.println("ERROR: Employee Details NOT Deleted or Do Not Exist");
                            break;

                        case "4":
                            //Get Employee Details from the User For Update
                        	System.out.println("Enter Employee Id to be Updated: \n");
                        	int updateEmpId = keyboard.nextInt();
                            System.out.println("Enter Updated Employee Name: \n");
                            String upEmpName = keyboard.next();
                            System.out.println("Enter Updated Employee Hours Worked: \n");
                            int upEmpHrsWrked = keyboard.nextInt();
                            System.out.println("Enter Updated Employee Rate of Pay: \n");
                            double upEmpRateofPay = keyboard.nextDouble();
                            System.out.println("Enter Updated Employee Phone Number: \n");
                            String upEmpPhoneNo = keyboard.next();
                            //Insert Employee Details into the database
                            boolean insertEmpUpResult = dao.insertUpdatedEmployeeDetailsAccount(updateEmpId, upEmpName, upEmpHrsWrked, upEmpRateofPay, upEmpPhoneNo);
                            if (insertEmpUpResult == true)
                                System.out.println("Employee Update Saved");
                            else
                                System.out.println("ERROR: Employee Update NOT Saved");
                            break;

                        case "99":
                            keepAppOpen = false;
                            System.out.println("Closing the Application");
                            break;

                        default:
                            System.out.println("No Valid Function Selected");
                            break;
                    } // end switch

                }// end if

                else if(selection.equals("4"))
                {
                    listInvoiceFunctionalityAvailable();
                    functionNumber = keyboard.nextLine();

                    switch (functionNumber) {

                        case "1":
                            //Get Invoice Details from the User
                            System.out.printf("Enter Invoice last name: \n");
                            String InvoicelName = keyboard.nextLine();
                            System.out.printf("Enter Invoice phone number: \n");
                            String InvoicephoneNo = keyboard.nextLine();
                            System.out.printf("Enter Invoice date: \n");
                            String Invoice_date = keyboard.nextLine();

                            Invoice invObj = new Invoice(Invoice_date, InvoicelName, InvoicephoneNo);

                            //Insert Invoice Details into the database
                            boolean insertResult = dao.insertInvoiceDetailsAccount(invObj);
                            if (insertResult == true)
                                System.out.println("Invoice Details Saved");
                            else
                                System.out.println("ERROR: Invoice Details NOT Saved");
                            break;

                        case "2":
                            //Retrieve ALL Invoice Records
                            ResultSet rSet = dao.retrieveAllInvoices();
                            if (rSet == null) {
                                System.out.println("No Records Found");
                                break;
                            }
                            else {
                                boolean tablePrinted =  printInvoiceTable(rSet);
                                if (tablePrinted == true)
                                    rSet.close();
                            }
                            break;

                        case "3":
                            //Delete Invoice by ID
                            System.out.println("Enter Invoice Id to be deleted or -99 to Clear all Rows");
                            String deleteInvId = keyboard.nextLine();
                            boolean deleteResult = dao.deleteInvoiceById(Integer.parseInt(deleteInvId));
                            if ((deleteResult == true) && (deleteInvId.equals("-99")))
                                System.out.println("Invoice Table Emptied");
                            else if (deleteResult == true)
                                System.out.println("Invoice Deleted");
                            else
                                System.out.println("ERROR: Invoice Details NOT Deleted or Do Not Exist");
                            break;

                        case "4":
                            //Get Invoice Details from the User For Update
                        	System.out.println("Enter Invoice Id to be Updated: \n");
                        	int updateInvId = keyboard.nextInt();
                            System.out.println("Enter Updated Invoice Last Name: \n");
                            String upInvName = keyboard.next();
                            System.out.println("Enter Updated Invoice Phone Number: \n");
                            String upInvPhoneNo = keyboard.next();
                            System.out.println("Enter Updated Invoice Date: \n");
                            String upInvDate = keyboard.next();
                            //Insert Customer Details into the database
                            boolean insertUpResult = dao.insertUpdatedInvoiceDetails(updateInvId, upInvName, upInvPhoneNo, upInvDate);
                            if (insertUpResult == true)
                                System.out.println("Invoice Update Saved");
                            else
                                System.out.println("ERROR: Invoice Update NOT Saved");
                            break;

                        case "99":
                            keepAppOpen = false;
                            System.out.println("Closing the Application");
                            break;

                        default:
                            System.out.println("No Valid Function Selected");
                            break;
                    } // end switch

                }// end if
                else if(selection.equals("5"))
                {
                    listDeliveryDocketFunctionalityAvailable();
                    functionNumber = keyboard.nextLine();

                    switch (functionNumber) {

                        case "1":
                            //Get Delivery Docket Details from the User
                        	System.out.printf("Enter Description: \n");
                            String orderDescription = keyboard.nextLine();
                            System.out.printf("Enter Address: \n");
                            String orderAddress = keyboard.nextLine();
                            System.out.printf("Enter Quantity: \n");
                            int orderQuantity = keyboard.nextInt();
                            System.out.printf("Enter Order Number: \n");
                            int orderNumber = keyboard.nextInt();
                            
                            
                            Delivery_Docket Delivery_Docket = new Delivery_Docket(orderDescription,orderQuantity,orderAddress, orderNumber);

                            //Insert Delivery Docket Details into the database
                            boolean insertResult = dao.insertDeliveryDocketDetailsAccount(Delivery_Docket);
                            if (insertResult == true)
                                System.out.println("Delivery Docket Details Saved");
                            else
                                System.out.println("ERROR: Delivery Docket Details NOT Saved");
                            break;

                        case "2":
                            //Retrieve ALL Delivery Dockets
                            ResultSet rSet = dao.retrieveAllDeliveryDockets();
                            if (rSet == null) {
                                System.out.println("No Records Found");
                                break;
                            }
                            else {
                                boolean tablePrinted = printDeliveryDocketTable(rSet);
                                if (tablePrinted == true)
                                    rSet.close();
                            }
                            break;

                        case "3":
                            //Delete Delivery Docket by ID
                            System.out.println("Enter Delivery Docket Id to be deleted or -99 to Clear all Rows");
                            String deleteArea_Id = keyboard.next();
                            boolean deleteResult = dao.deleteDeliveryDocketById(Integer.parseInt(deleteArea_Id));
                            if ((deleteResult == true) && (deleteArea_Id.equals("-99")))
                                System.out.println("Delivery Docket Table Emptied");
                            else if (deleteResult == true)
                                System.out.println("Delivery Docket Deleted");
                            else
                                System.out.println("ERROR: Delivery Docket Details NOT Deleted or Do Not Exist");
                            break;

                        case "4":
                            //Get Delivery Docket Details from the User For Update
                        	System.out.printf("Enter Area ID to Update: \n");
                            int updateAreaId = keyboard.nextInt();
                            keyboard.nextLine();
                        	System.out.printf("Enter Updated Description: \n");
                            String upOrderDescription = keyboard.nextLine();
                            System.out.printf("Enter Updated Address: \n");
                            String upOrderAddress = keyboard.nextLine();
                            System.out.printf("Enter Updated Quantity: \n");
                            int upOrderQuantity = keyboard.nextInt();
                            System.out.printf("Enter Updated Order Number: \n");
                            int upOrderNumber = keyboard.nextInt();
                            
                            //Insert Delivery Docket Details into the database
                            boolean insertUpResult = dao.insertDeliveryDocketDetailsAccountUpdate(updateAreaId, upOrderDescription, upOrderQuantity, upOrderAddress, upOrderNumber);
                            if (insertUpResult == true)
                                System.out.println("Delivery Docket Update Saved");
                            else
                                System.out.println("ERROR: Delivery Docket Update NOT Saved");
                            break;
                            
                        case "99":
                            keepAppOpen = false;
                            System.out.println("Closing the Application");
                            break;

                        default:
                            System.out.println("No Valid Function Selected");
                            break;
                    } // end switch

                }// end if
                
                if(selection.equals("6"))
                {
                    listDeliveryFunctionalityAvailable();
                    functionNumber = keyboard.nextLine();

                    switch (functionNumber) {

                        case "1":
                            //Get Delivery Details from the User
                            System.out.println("Enter Delivery Date: \n");
                            String delDate = keyboard.nextLine();
                            System.out.println("Enter Delivery Customer ID: \n");
                            int delCusId = keyboard.nextInt();
                            System.out.println("Enter Delivery Quantity: \n");
                            int delQuan = keyboard.nextInt();

                            Delivery delObj = new Delivery(delDate, delQuan, delCusId);

                            //Insert Delivery Details into the database
                            boolean insertResult = dao.insertDeliveryDetailsAccount(delObj);
                            if (insertResult == true)
                                System.out.println("Delivery Details Saved");
                            else
                                System.out.println("ERROR: Delivery Details NOT Saved");
                            break;

                        case "2":
                            //Retrieve ALL Delivery Records
                            ResultSet rSet = dao.retrieveAllDeliverys();
                            if (rSet == null) {
                                System.out.println("No Records Found");
                                break;
                            }
                            else {
                                boolean tablePrinted = printDeliveryTable(rSet);
                                if (tablePrinted == true)
                                    rSet.close();
                            }
                            break;

                        case "3":
                            //Delete Delivery Record by ID
                            System.out.println("Enter Delivery Id to be deleted or -99 to Clear all Rows");
                            String deletedelId = keyboard.next();
                            boolean deleteResult = dao.deleteDeliverysById(Integer.parseInt(deletedelId));
                            if ((deleteResult == true) && (deletedelId.equals("-99")))
                                System.out.println("Delivery Table Emptied");
                            else if (deleteResult == true)
                                System.out.println("Delivery Deleted");
                            else
                                System.out.println("ERROR: Delivery Details NOT Deleted or Do Not Exist");
                            break;

                        case "4":
                            //Get Delivery Details from the User For Update
                            System.out.println("Enter Delivery ID To Be Updated: \n");
                        	int updateDelId = keyboard.nextInt();
                            System.out.println("Enter Delivery Date: \n");
                            String upDelDate = keyboard.next();
                            keyboard.nextLine();
                            System.out.println("Enter Delivery Quantity: \n");
                            int upDelQuan = keyboard.nextInt();
                            System.out.println("Enter Delivery Customer ID: \n");
                            int upDelCusId = keyboard.nextInt();
                            
                            //Insert Customer Details into the database
                            boolean insertUpResult = dao.insertDeliveryDetailsAccountUpdate(updateDelId, upDelDate, upDelQuan, upDelCusId);
                            if (insertUpResult == true)
                                System.out.println("Delivery Update Saved");
                            else
                                System.out.println("ERROR: Delivery Update NOT Saved");
                            break;
                            
                        case "99":
                            keepAppOpen = false;
                            System.out.println("Closing the Application");
                            break;

                        default:
                            System.out.println("No Valid Function Selected");
                            break;
                    } // end switch

                }// end if
                
                /*if(selection.equals("7"))
                {
                    listBillsFunctionalityAvailable();
                    functionNumber = keyboard.nextLine();

                    switch (functionNumber) {

                        case "1":
                            //Get Bills Details from the User
                            System.out.println("Enter Method Of Payment: \n");
                            String methodOfPayment = keyboard.nextLine();
                            System.out.println("Enter Date: \n");
                            String billDate = keyboard.nextLine();
                            System.out.println("Enter Customer ID: \n");
                            int cusId = keyboard.nextInt();

                            Bills billsObj = new Bills(methodOfPayment,billDate,cusId);

                            //Insert Customer Details into the database
                            boolean insertResult = dao.insertBillsDetailsAccount(billsObj);
                            if (insertResult == true)
                                System.out.println("Bills Details Saved");
                            else
                                System.out.println("ERROR: Bills Details NOT Saved");
                            break;

                        case "2":
                            //Retrieve ALL Bills Records
                            ResultSet rSet = dao.retrieveAllBillsAccounts();
                            if (rSet == null) {
                                System.out.println("No Records Found");
                                break;
                            }
                            else {
                                boolean tablePrinted = printBillsTable(rSet);
                                if (tablePrinted == true)
                                    rSet.close();
                            }
                            break;

                        case "3":
                            //Delete Bills Record by ID
                            System.out.println("Enter Bills Id to be deleted or -99 to Clear all Rows");
                            String deletebillId = keyboard.next();
                            boolean deleteResult = dao.deleteCustomerById(Integer.parseInt(deletebillId));
                            if ((deleteResult == true) && (deletebillId.equals("-99")))
                                System.out.println("Bills Table Emptied");
                            else if (deleteResult == true)
                                System.out.println("Bills Deleted");
                            else
                                System.out.println("ERROR: Bills Details NOT Deleted or Do Not Exist");
                            break;

                        case "4":
                            //Get Bills Details from the User For Update
                        	System.out.println("Enter Bill ID To Update: \n");
                            int upBillId = keyboard.nextInt();
                            keyboard.nextLine();
                        	System.out.println("Enter UpdatedMethod Of Payment: \n");
                            String upMethodOfPayment = keyboard.nextLine();
                            System.out.println("Enter Updated Date: \n");
                            String upBillDate = keyboard.nextLine();
                            System.out.println("Enter Updated Customer ID: \n");
                            int upCusId = keyboard.nextInt();
                            
                            //Insert Bills Details into the database
                            boolean insertUpResult = dao.insertUpdatedBillsDetailsAccount(upBillId, upMethodOfPayment, upBillDate, upCusId);
                            if (insertUpResult == true)
                                System.out.println("Bills Update Saved");
                            else
                                System.out.println("ERROR: Bills Update NOT Saved");
                            break;
                            
                        case "99":
                            keepAppOpen = false;
                            System.out.println("Closing the Application");
                            break;

                        default:
                            System.out.println("No Valid Function Selected");
                            break;
                    } // end switch

                }// end if*/

            }// end while

            //Tidy up Resources
            keyboard.close();

        }

        catch(Exception e) {
            System.out.println("PROGRAM TERMINATED - ERROR MESSAGE:" + e.getMessage());
        } // end try-catch


    } // end main


}
