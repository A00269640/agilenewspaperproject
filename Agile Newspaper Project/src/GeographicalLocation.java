
public class  GeographicalLocation {
	
	private int id;
	
	void setId(int zoneId) {
		id = zoneId;
	}
	
	
	int getId() {
		return id;
	}
	
public GeographicalLocation(int zoneId) throws GeographicalLocationsExceptionHandler  {
		
		id = 0;
		
		// Validate Input
		try {
			
			validateZoneId(zoneId);
			
		}
		catch (GeographicalLocationsExceptionHandler e) {
			throw e;
		}
		
		// Set Attributes
		id = zoneId;
	}
	
	public static void validateZoneId(int zoneId) throws GeographicalLocationsExceptionHandler {
		
		if (zoneId < 0)
			throw new GeographicalLocationsExceptionHandler("Zone ID is Not a Valid Integer!");
	}
	
}
