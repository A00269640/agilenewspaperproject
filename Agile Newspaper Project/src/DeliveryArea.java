
public class DeliveryArea
{

	private int id;
	private String description;
	private String address;

	void setId(int area_Id)
	{
		id = area_Id;
	}

	void setDescription(String houseDescription)
	{
		description = houseDescription;
	}

	void setAddress(String houseAddress)
	{
		address = houseAddress;
	}
		int getId ()
		{
			return id;
		}

		String getDescription ()
		{
			return description;
		}

		String getAddress ()
		{
			return address;
		}

	public DeliveryArea(String houseDescription, String houseAddress) throws DeliveryAreaExceptionHandler
	{

			id = 0;

			// Validate Input
			try
			{

				validateDescription(houseDescription);
				validateAddress(houseAddress);

			}
			catch (DeliveryAreaExceptionHandler e)
			{
				throw e;
			}

			// Set Attributes
			description = houseDescription;
			address = houseAddress;
		}

		public static void validateDescription (String houseDescription) throws DeliveryAreaExceptionHandler
		{

			//Agree Formating Rules on "House Description"
			//E.G. Name String must be a minimum of 2 characters and a maximum of 100 characters

			if (houseDescription.isBlank() || houseDescription.isEmpty())
				throw new DeliveryAreaExceptionHandler("Description NOT specified");
			else if (houseDescription.length() < 2)
				throw new DeliveryAreaExceptionHandler("Description does not meet minimum length requirements");
			else if (houseDescription.length() > 100)
				throw new DeliveryAreaExceptionHandler("Description exceeds maximum length requirements");

		}

		public static void validateAddress (String houseAddress) throws DeliveryAreaExceptionHandler
		{

			//Agree Formating Rules on "House Address"
			//E.G. Name String must be a minimum of 5 characters and a maximum of 60 characters

			if (houseAddress.isBlank() || houseAddress.isEmpty())
				throw new DeliveryAreaExceptionHandler("House Address NOT specified");
			else if (houseAddress.length() < 5)
				throw new DeliveryAreaExceptionHandler("House Address does not meet minimum length requirements");
			else if (houseAddress.length() > 60)
				throw new DeliveryAreaExceptionHandler("House Address exceeds maximum length requirements");


		}

}